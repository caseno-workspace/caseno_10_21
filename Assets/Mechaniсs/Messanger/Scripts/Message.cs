using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Message : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI TextMessage;
    [SerializeField] private Image SenderAvatar;
    [SerializeField] private TextMeshProUGUI SendingTime;

    public void SetMessageInfo(string textMessage, Sprite senderAvatar, string sendingTime)
    {
        TextMessage.text = textMessage;
        SenderAvatar.sprite = senderAvatar;
        SendingTime.text = sendingTime.ToString();
    }
}