using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompositionRoot : MonoBehaviour
{
    private SpreadsheetsLoader _spreadsheetsLoader;
    private void Awake()
    {
        SetSpreadsheetLoader();
    }

    private void SetSpreadsheetLoader()
    {
        _spreadsheetsLoader = new SpreadsheetsLoader();
        _spreadsheetsLoader.Load();
        _spreadsheetsLoader.SetCurrentLanguage(SpreadsheetsLoader.Language.Russian);
    }

    void Start()
    {
        
    }
}
