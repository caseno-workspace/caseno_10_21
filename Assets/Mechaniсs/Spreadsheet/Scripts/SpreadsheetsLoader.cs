using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpreadsheetsLoader
{
    private static int _currentLanguage;
    private static string[,] _localization;

    #region Init

    public void Load()
    {
        TextAsset csvFile = Resources.Load<TextAsset>("Spreadsheets/Localization");
        _localization = SpreadsheetsLoader.GetCSVGrid(csvFile.text);
    }

    public void SetCurrentLanguage(Language newLanguege)
    {
        var language = newLanguege.ToString();
        for (int i = 0; i < _localization.GetLength(1); i++)
        {
            if (_localization[0, i] == language)
            {
                _currentLanguage = i;
                return;
            }
        }
    }

    #endregion

    public static string GetText(string key)
    {
        for (int i = 0; i < _localization.GetLength(0); i++)
        {
            if (_localization[i, 0] == key)
            {
                return _localization[i, _currentLanguage];
            }
        }
        Debug.LogError("Запрошен несуществующий ключ текста");
        return "";
    }

    static public string[,] GetCSVGrid(string csvText)
    {
        string[] lines = csvText.Split("\n"[0]);

        int totalColumns = 0;
        for (int i = 0; i < lines.Length; i++)
        {
            string[] row = lines[i].Split(',');
            totalColumns = Mathf.Max(totalColumns, row.Length);
        }

        string[,] outputGrid = new string[totalColumns + 1, lines.Length + 1];
        for (int y = 0; y < lines.Length; y++)
        {
            string[] row = lines[y].Split(',');
            for (int x = 0; x < row.Length; x++)
            {
                outputGrid[x, y] = row[x];
            }
        }

        return outputGrid;
    }

    public enum Language
    {
        Russian,
        English,
    }
}
