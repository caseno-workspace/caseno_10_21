using UnityEditorInternal;
using UnityEngine;
using XNode;
using XNodeEditor;

namespace Mechaniсs.DialogSystem.Scripts
{
    [CustomNodeEditor(typeof(DialogSegment))]
    public class DialogNodeEditor : NodeEditor
    {
        public override void OnBodyGUI()
        {
            serializedObject.Update();

            var segment = serializedObject.targetObject as DialogSegment;
            NodeEditorGUILayout.PortField(segment.GetPort("input"));

            GUILayout.Label("DialogText");
            segment.DialogText = GUILayout.TextArea(segment.DialogText, new GUILayoutOption[]
            {
                GUILayout.MinHeight(50)
            });

            NodeEditorGUILayout.DynamicPortList(
                "Answers",
                typeof(string),
                serializedObject,
                NodePort.IO.Input,
                Node.ConnectionType.Override,
                Node.TypeConstraint.None,
                CreateReorderableList
            );

            foreach (var dynamicPort in target.DynamicPorts)
            {
                if (NodeEditorGUILayout.IsDynamicPortListPort(dynamicPort))
                    continue;

                NodeEditorGUILayout.PortField(dynamicPort);
            }

            serializedObject.ApplyModifiedProperties();
        }

        private void CreateReorderableList(ReorderableList list)
        {
            list.elementHeightCallback = (int index) => { return 60; };
            
            // Override drawHeaderCallback to display node's name instead
            list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                var segment = serializedObject.targetObject as DialogSegment;

                NodePort port = segment.GetPort( "Answers " + index);

                segment.Answers[index] = GUI.TextArea(rect, segment.Answers[index]);

                if (port != null) {
                    Vector2 pos = rect.position + (port.IsOutput?new Vector2(rect.width + 6, 0) : new Vector2(-36, 0));
                    NodeEditorGUILayout.PortField(pos, port);
                }
            };
        }
    }
}
