using System;
using System.Collections.Generic;
using System.ComponentModel;
using XNode;

namespace Mechaniсs.DialogSystem
{
    [Serializable]
    public struct Connections
    {
    }

    public class DialogSegment : Node
    {
        [Input] 
        public Connections input;

        public string DialogText;

        [Output(dynamicPortList = true)] 
        public List<string> Answers;

        public override object GetValue(NodePort port)
        {
            return null;
        }
    }
}
