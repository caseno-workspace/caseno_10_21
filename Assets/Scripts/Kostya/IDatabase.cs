﻿using System;
using System.Collections;
using UnityEngine;

namespace DataBase
{
    public interface IDatabase
    {
        Action OnDataChanged { get; set; }
        void SetData<T>(T data);
        T GetData<T>();
    }
}