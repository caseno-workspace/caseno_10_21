using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using DataBase;

[CustomEditor(typeof(EvidenceData))]
public class EvidenceDataGIUbutton : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EvidenceData evidenceData = (EvidenceData)target;

        if (GUILayout.Button("Refresh", EditorStyles.miniButton))
        {
            evidenceData.OnDataChanged?.Invoke();
        }
    }
}
