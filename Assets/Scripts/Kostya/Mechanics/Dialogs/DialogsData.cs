using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataBase
{
    [CreateAssetMenu()]
    public class DialogsData : ScriptableObject, IDatabase
    {
        public Action OnDataChanged { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public T GetData<T>()
        {
            throw new NotImplementedException();
        }

        public void SetData<T>(T data)
        {
            OnDataChanged?.Invoke();
            throw new NotImplementedException();
        }
    }
}
