﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataBase
{
    [CreateAssetMenu()]
    public class EvidenceData : ScriptableObject, IDatabase
    {
        public List<Evidence> Evidances = new List<Evidence>();
        public Action OnDataChanged { get;  set; }

        public T GetData<T>()
        {
            throw new NotImplementedException();
        }

        public void SetData<T>(T data)
        {
            OnDataChanged?.Invoke();
            throw new NotImplementedException();
        }

    }
}

//public class Evidence : ScriptableObject
//{
//    public bool Found;
//    public string Name;
//    public Sprite Picture;
//    public string Description;

//    public (bool, string, Sprite,string) GetSlotInfo()
//    {
//        return (Found, Name, Picture, Description);
//    }
//}
