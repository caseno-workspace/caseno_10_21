using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu()]
public class Evidence : ScriptableObject
{
    public bool Found;
    public string Name;
    public Sprite Picture;
    [TextArea]
    public string Description;    

    public (bool, string, Sprite, string) GetSlotInfo()
    {
        return (Found, Name, Picture, Description);
    }
}


