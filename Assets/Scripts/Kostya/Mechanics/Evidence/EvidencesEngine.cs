﻿using Engine;
using View;
using DataBase;
using System.Collections.Generic;

namespace Engines
{
    public class EvidencesEngine : IEngine
    {
        /// <summary>
        /// взаимодействие с UI
        /// </summary>
        EvidenceMenu _evidenceMenu;
        /// <summary>
        /// взаимодействие с DataBase
        /// </summary>
        EvidenceData _evidenceData;

        private List<Evidence> _currentEvidances;
        public EvidencesEngine(EvidenceData evidenceData, EvidenceMenu evidanceMenu)
        {
            _evidenceData = evidenceData;
            _evidenceMenu = evidanceMenu;

            _currentEvidances = _evidenceData.Evidances;
            _evidenceData.OnDataChanged += _evidenceMenu.UpdateView;
        }
        public void Enable()
        {            
            //пробрасываем полученные в конструторе данные из evidenceData в evidanceMenu через его метод SetInfo
            _evidenceMenu.SetInfo(_evidenceData.Evidances);
        }

        public void Disable()
        {
            //throw new System.NotImplementedException();
        }

        public void ChangeData()
        {
            _evidenceData.SetData<List<Evidence>>(_currentEvidances);
        }
    }
}