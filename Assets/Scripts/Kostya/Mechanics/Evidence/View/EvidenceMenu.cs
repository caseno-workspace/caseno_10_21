using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace View
{
    public class EvidenceMenu : MonoBehaviour, IView
    {
        [SerializeField] private List<EvidenceSlot> EvidanceSlots = new List<EvidenceSlot>();
        private Evidence[] _currentEvidences;

        /// <summary>
        /// ����� (�� EvidencesEngine) ��������� �����, ������� �� � ������ _currentEvidences, ���������
        /// </summary>
        /// <param name="evidences"></param>
        public void SetInfo(List<Evidence> evidences)
        {
            _currentEvidences = evidences.ToArray();
            //������ ����
            UpdateView();
        }

        /// <summary>
        /// �� ������� ���� �������� ���� � ������� ��� ������ GetSlotInfo.
        /// � � ���������� ����� ��������� �����, ������� ��� ������� ��� ������ SetEvidenceInfo
        /// </summary>
        public void UpdateView()
        {
            foreach (var slot in EvidanceSlots)
            {
                slot.gameObject.SetActive(false);
            }

            for (int i = 0; i < _currentEvidences.Length; i++)            {
                
                (bool found, string name, Sprite Picture, string description) = _currentEvidences[i].GetSlotInfo();
                
                if (found)
                {
                    EvidanceSlots[i].SetEvidenceInfo(name, Picture, description);
                    EvidanceSlots[i].gameObject.SetActive(true);
                }
                //else
                    //EvidanceSlots[i].gameObject.SetActive(false);
            }
        }
    }
}
