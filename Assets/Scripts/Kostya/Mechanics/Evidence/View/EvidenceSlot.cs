using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using View;

public class EvidenceSlot : MonoBehaviour
{
    [SerializeField] private string Name;
    [SerializeField] private Image Picture;
    [SerializeField] private Text Description;

    public void SetEvidenceInfo(string name, Sprite picture, string description)
    {        
        Name = name;
        Picture.sprite = picture;
        Description.text = description;
    }
}
