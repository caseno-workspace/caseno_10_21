using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DataBase;


namespace Case
{
    [CreateAssetMenu()]
    public class CaseData : ScriptableObject
    {
        public string CaseName;
        public EvidenceData evidenceData;
        public DialogsData dialogsData;
    }
}
