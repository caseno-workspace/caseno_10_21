using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Engine
{
    public interface IEngine
    {
        void Enable();
        void Disable();
    }
}
