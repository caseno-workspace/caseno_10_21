﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Engines;
using DataBase;
using Case;
using Engine;
using View;

namespace Root
{
    public class GameCompositionRoot : MonoBehaviour
    {
        //public DialogMenu DialogView;
        public EvidenceMenu EvidanceMenu;

        private CaseData _currentCaseData;
        private Object[] _casesData;
        private List<IEngine> _engines = new List<IEngine>();

        private void Awake()
        {
            if(!LoadSave())
                _casesData = Resources.LoadAll<CaseData>("Cases");
            //здесь в _casesData данные есть!
        }

        void Start()
        {
            _currentCaseData = _casesData[0] as CaseData;

            InitEngines();
        }

        private void InitEngines()
        {
            //var dialogEngine = new DialogEngine(_currentCaseData.dialogsData, DialogView);
            var evidencesEngine = new EvidencesEngine(_currentCaseData.evidenceData, EvidanceMenu); //пробросили связь базы данных и UI окон

            //_engines.Add(dialogEngine);
            _engines.Add(evidencesEngine);

            foreach (IEngine engine in _engines)
            {
                engine.Enable();
            }
        }


        private bool LoadSave()
        {
            _casesData = null;
            return false;
        }

        private void OnDisable()
        {
            foreach (var engine in _engines)
            {
                engine.Disable();
            }
        }
    }
}