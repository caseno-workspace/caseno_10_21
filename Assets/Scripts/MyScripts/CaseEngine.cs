using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CaseEngine : MonoBehaviour
{
    [SerializeField]
    private Transform UITextBlocksContainer;
    [SerializeField]
    private GameObject textBlockVisualisationPrefab;

    private TextBlock [] caseTextBlocks;

    private void Start()
    {
        caseTextBlocks = transform.GetComponentsInChildren<TextBlock>();
        
        if (caseTextBlocks.Length > 0)
        {
            foreach (var item in caseTextBlocks)
            {
                if (item.IsUncovered) CreateNewTextBlockOnUI(item); //���� �������� ������� ���������� - ������� �� �� ����� �������������
                else item.OnUncoveringChanged += CreateNewTextBlockOnUI; //���� �������� �������, �� ������������� �� ������� �� ��������
            }
        }

    }

    private void CreateNewTextBlockOnUI(TextBlock item)
    {
        GameObject newTextBlockVisualisation = Instantiate(textBlockVisualisationPrefab, UITextBlocksContainer.transform);
        Text newTextBlockVisualisationText = newTextBlockVisualisation.GetComponentInChildren<Text>();
        newTextBlockVisualisationText.text = item.TextAll;
    }

    
}
