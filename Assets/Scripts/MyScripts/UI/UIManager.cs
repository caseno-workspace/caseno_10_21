﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private RectTransform dataBaseWindow;
    [SerializeField]
    private RectTransform boardWindow;
    [SerializeField]    
    private int taskBarHeight;

    private bool isDataBaseWindowOpen = false;
    private bool isBoardWindowOpen = false;
    

    public void ButtonDataBase()
    {
        if (!isDataBaseWindowOpen) 
        {
            if (!isBoardWindowOpen)
            {
                dataBaseWindow.DOAnchorPos(new Vector2(0, taskBarHeight / 2), 0.25f);
                isDataBaseWindowOpen = true;
            }
            else
            {
                boardWindow.DOAnchorPos(new Vector2(0, -1080 + taskBarHeight / 2), 0.25f);
                isBoardWindowOpen = false;
                dataBaseWindow.DOAnchorPos(new Vector2(0, taskBarHeight / 2), 0.25f).SetDelay(0.3f);
                isDataBaseWindowOpen = true;
            }
        }
        else
        {
            dataBaseWindow.DOAnchorPos(new Vector2(0, -1080 + taskBarHeight/2), 0.25f);
            isDataBaseWindowOpen = false;
        }
    }

    public void ButtonBoard()
    {
        if (!isBoardWindowOpen)
        {
            if (!isDataBaseWindowOpen)
            {
                boardWindow.DOAnchorPos(new Vector2(0, taskBarHeight / 2), 0.25f);
                isBoardWindowOpen = true;
            }
            else
            {
                dataBaseWindow.DOAnchorPos(new Vector2(0, -1080 + taskBarHeight / 2), 0.25f);
                isDataBaseWindowOpen = false;
                boardWindow.DOAnchorPos(new Vector2(0, taskBarHeight / 2), 0.25f).SetDelay(0.3f);
                isBoardWindowOpen = true;
            }
        }
        else
        {
            boardWindow.DOAnchorPos(new Vector2(0, -1080 + taskBarHeight / 2), 0.25f);
            isBoardWindowOpen = false;
        }
    }

    public void ButtonMessenger()
    {


    }
    public void ButtonQuestionList()
    {

    }
}
