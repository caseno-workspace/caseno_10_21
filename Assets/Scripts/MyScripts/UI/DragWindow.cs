﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragWindow : MonoBehaviour, IDragHandler
{
    private RectTransform parentTransform;
    private Button parentButton;
    private CanvasScaler mainCanvasScaler;
    private RectTransform rectTransform;

    public void OnDrag(PointerEventData eventData)
    {
        if (
            Input.mousePosition.x < Screen.width &&
            Input.mousePosition.x > 0 &&
            Input.mousePosition.y < Screen.height &&
            Input.mousePosition.y > 90
            )
        {
            //коэффициент соотношения сторон предпочитаемый
            float k = mainCanvasScaler.referenceResolution.y / mainCanvasScaler.referenceResolution.x;

            parentTransform.anchoredPosition += new Vector2(
                eventData.delta.x / Screen.width * mainCanvasScaler.referenceResolution.x * Screen.width / Screen.height * k,
                eventData.delta.y / Screen.height * mainCanvasScaler.referenceResolution.y);
            parentButton.onClick?.Invoke();
        }
    }
    
    void Start()
    {
        mainCanvasScaler = GetComponentInParent<CanvasScaler>();
        rectTransform = GetComponent<RectTransform>();
        parentTransform = transform.parent.GetComponent<RectTransform>();
        parentButton = transform.parent.GetComponent<Button>();
    }
}
