﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpSystem : MonoBehaviour
{
    /// <summary>
    /// Родительский объект для отображения всплывающего окна ПОД другими
    /// </summary>
    [SerializeField]
    private GameObject bottomLocated;
    /// <summary>
    /// Родительский объект для отображения всплывающего окна НАД другими
    /// </summary>
    [SerializeField]
    private GameObject topLocated;

    [SerializeField]
    private GameObject messengerWindow;
    [SerializeField]
    private GameObject questionListWindow;

    
    public void SetMessengerUpper()
    {
        {
            messengerWindow.transform.SetParent(topLocated.transform);
            questionListWindow.transform.SetParent(bottomLocated.transform);
        }
    }
    public void SetQuestionListUpper()
    {
        {
            questionListWindow.transform.SetParent(topLocated.transform);
            messengerWindow.transform.SetParent(bottomLocated.transform);
        }
    }

    public void ButtonMessenger()
    {
        if (!messengerWindow.activeInHierarchy)
        {
            messengerWindow.SetActive(true);
            messengerWindow.GetComponent<Button>().onClick?.Invoke();
        }
            
        else messengerWindow.SetActive(false);
    }

    public void ButtonQuestionList()
    {
        if (!questionListWindow.activeInHierarchy)
        {
            questionListWindow.SetActive(true);
            questionListWindow.GetComponent<Button>().onClick?.Invoke();
        }
            
        else questionListWindow.SetActive(false);
    }
}
