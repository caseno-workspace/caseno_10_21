using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TextBlock))]
public class TextBlockGIUbutton : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        TextBlock textBlock = (TextBlock)target;

        if (GUILayout.Button("Set Uncovered", EditorStyles.miniButton))
        {
            textBlock.IsUncovered = true;
        }
    }
}
