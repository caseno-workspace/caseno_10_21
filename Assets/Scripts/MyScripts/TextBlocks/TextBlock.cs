using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TextBlock : MonoBehaviour
{
    [SerializeField, TextArea]
    private string textAll;

    public string TextAll { get { return textAll; } }

    [SerializeField, TextArea]
    private string summary;

    public string Summary { get { return summary; } }

    [SerializeField]
    private bool isUncovered = false;

    public bool IsUncovered
    {
        get { return isUncovered; }
        set 
        {
            Debug.Log("Open");
            if (value == true && isUncovered == false) OnUncoveringChanged?.Invoke(this); //���� �������� ��������������� ������ "�������", �� �������� ������� ��������� ��������
            isUncovered = value;
        }
    }

    public Action <TextBlock> OnUncoveringChanged;
}
